FROM golang:1.23 AS builder

RUN apt-get update && apt-get install -y \
  libicu-dev \
  && rm -rf /var/lib/apt/lists/*

WORKDIR /src
COPY . .

RUN make

FROM debian:12

RUN apt-get update && apt-get install -y \
  libicu72 \
  && rm -rf /var/lib/apt/lists/*

COPY --from=builder /src/bin/gitlab-elasticsearch-indexer /usr/local/bin/gitlab-elasticsearch-indexer

ENTRYPOINT ["/usr/local/bin/gitlab-elasticsearch-indexer"]
